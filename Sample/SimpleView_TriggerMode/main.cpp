
#include "../common/common.hpp"

BOOL g_bThreadRunning = FALSE;
void*  __stdcall ProcessThread(void* pHandle)
{
    int nRet = MV3D_RGBD_OK;
    if (NULL != pHandle)
    {
        while (g_bThreadRunning)
        {
            ASSERT_OK(MV3D_RGBD_SoftTrigger(pHandle));
            LOGD("MV3D_RGBD_SoftTrigger success!");
            Sleep(3000);
        }
    }
    return NULL;
}

int main(int argc,char** argv)
{
    MV3D_RGBD_VERSION_INFO stVersion;
    ASSERT_OK( MV3D_RGBD_GetSDKVersion(&stVersion) );
    LOGD("dll version: %d.%d.%d", stVersion.nMajor, stVersion.nMinor, stVersion.nRevision);

    ASSERT_OK(MV3D_RGBD_Initialize());

    unsigned int nDevNum = 0;
    ASSERT_OK(MV3D_RGBD_GetDeviceNumber(DeviceType_Ethernet | DeviceType_USB, &nDevNum));
    LOGD("MV3D_RGBD_GetDeviceNumber success! nDevNum:%d.", nDevNum);
    ASSERT(nDevNum);

    // 查找设备
    std::vector<MV3D_RGBD_DEVICE_INFO> devs(nDevNum);
    ASSERT_OK(MV3D_RGBD_GetDeviceList(DeviceType_Ethernet | DeviceType_USB, &devs[0], nDevNum, &nDevNum));
    for (unsigned int i = 0; i < nDevNum; i++)
    {
        LOG("Index[%d]. SerialNum[%s] IP[%s] name[%s].\r\n", i, devs[i].chSerialNumber, devs[i].SpecialInfo.stNetInfo.chCurrentIp, devs[i].chModelName);
    }

    //打开设备
    void* handle = NULL;
    unsigned int nIndex = 0;
    ASSERT_OK(MV3D_RGBD_OpenDevice(&handle, &devs[nIndex]));
    LOGD("OpenDevice success.");

    HANDLE hProcessThread = NULL;  //软触发线程
    hProcessThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ProcessThread, handle, 0, NULL);
    if (NULL == hProcessThread)
    {
        LOGD("Create proccess Thread failed ");
    }
    g_bThreadRunning = TRUE;

    // 开始工作流程
    ASSERT_OK(MV3D_RGBD_Start(handle));
    LOGD("Start work success.");

    MV3D_RGBD_FRAME_DATA stFrameData = {0};
    BOOL bExit_Main = FALSE;
    while (!bExit_Main)
    {
        // 获取图像数据
        int nRet = MV3D_RGBD_FetchFrame(handle, &stFrameData, 5000);
        if (MV3D_RGBD_OK == nRet)
        {
            for(int i = 0; i < stFrameData.nImageCount; i++)
            {
               LOGD("MV3D_RGBD_FetchFrame success: framenum (%d) height(%d) width(%d)  len (%d)!", stFrameData.stImageData[i].nFrameNum,
                stFrameData.stImageData[i].nHeight, stFrameData.stImageData[i].nWidth, stFrameData.stImageData[i].nDataLen);
            }
        }

        //按任意键退出
        if (_kbhit())
        {
            bExit_Main = TRUE;
        }
    }

    ASSERT_OK(MV3D_RGBD_Stop(handle));

    if (NULL != hProcessThread)
    {
        g_bThreadRunning = FALSE;
        WaitForSingleObject(hProcessThread,INFINITE);
        CloseHandle(hProcessThread);
        hProcessThread = NULL;
    }
    
    ASSERT_OK(MV3D_RGBD_CloseDevice(&handle));
    ASSERT_OK(MV3D_RGBD_Release());
    
    LOGD("Main done!");
    return  0;
}

