
#include "../common/common.hpp"

int main(int argc,char** argv)
{
    MV3D_RGBD_VERSION_INFO stVersion;
    ASSERT_OK( MV3D_RGBD_GetSDKVersion(&stVersion) );
    LOGD("dll version: %d.%d.%d", stVersion.nMajor, stVersion.nMinor, stVersion.nRevision);

    ASSERT_OK(MV3D_RGBD_Initialize());

    unsigned int nDevNum = 0;
    ASSERT_OK(MV3D_RGBD_GetDeviceNumber(DeviceType_Ethernet | DeviceType_USB, &nDevNum));
    LOGD("MV3D_RGBD_GetDeviceNumber success! nDevNum:%d.", nDevNum);
    ASSERT(nDevNum);

    // 查找设备
    std::vector<MV3D_RGBD_DEVICE_INFO> devs(nDevNum);
    ASSERT_OK(MV3D_RGBD_GetDeviceList(DeviceType_Ethernet | DeviceType_USB, &devs[0], nDevNum, &nDevNum));
    for (unsigned int i = 0; i < nDevNum; i++)
    {  
        LOG("Index[%d].", i);
        LOG("SerialNum[%s] IP[%s] name[%s] DeviceVersion[%s].\r\n", devs[i].chSerialNumber, devs[i].SpecialInfo.stNetInfo.chCurrentIp, devs[i].chModelName, devs[i].chDeviceVersion);

        LOG("******************Detail Info******************");
        LOG("ManufacturerName[%s]  ManufacturerSpecificInfo[%s] UserDefinedName[%s] MacAddress[%x:%x:%x:%x:%x:%x] enIPCfgMode[%d] CurrentSubNetMask[%s] DefultGateWay[%s] NetExport[%s].",
            devs[i].chManufacturerName, devs[i].chManufacturerSpecificInfo, devs[i].chUserDefinedName, devs[i].SpecialInfo.stNetInfo.chMacAddress[0], devs[i].SpecialInfo.stNetInfo.chMacAddress[1], 
            devs[i].SpecialInfo.stNetInfo.chMacAddress[2], devs[i].SpecialInfo.stNetInfo.chMacAddress[3], devs[i].SpecialInfo.stNetInfo.chMacAddress[4], devs[i].SpecialInfo.stNetInfo.chMacAddress[5],
            devs[i].SpecialInfo.stNetInfo.enIPCfgMode, devs[i].SpecialInfo.stNetInfo.chCurrentSubNetMask, devs[i].SpecialInfo.stNetInfo.chDefultGateWay, devs[i].SpecialInfo.stNetInfo.chNetExport);

        if (IpCfgMode_Static & devs[i].SpecialInfo.stNetInfo.enIPCfgMode)
        {
            LOG("IPCfgMode[STATIC]");
        } 
        if (IpCfgMode_DHCP & devs[i].SpecialInfo.stNetInfo.enIPCfgMode)
        {
            LOG("IPCfgMode[DHCP]");
        } 
        if (IpCfgMode_LLA & devs[i].SpecialInfo.stNetInfo.enIPCfgMode)
        {
            LOG("IPCfgMode[LLA]");
        }

        LOG("***********************************************\r\n",);
    }

    unsigned int nIndex = 0;
    FILE* pfile;
    char filename[256] = "CurrentDeviceInfo.txt";
    pfile = fopen(filename, "wb");
    if (pfile != NULL)
    {
        char chDevInfo[526] = "";
		if (DeviceType_Ethernet == devs[nIndex].enDeviceType)
		{
            sprintf(chDevInfo, "SerialNum[%s] IP[%s] name[%s] DeviceVersion[%s].\r\nManufacturerName[%s] ManufacturerSpecificInfo[%s] UserDefinedName[%s] MacAddress[%x:%x:%x:%x:%x:%x] CurrentSubNetMask[%s] DefultGateWay[%s] NetExport[%s].\r\n",
               devs[nIndex].chSerialNumber, devs[nIndex].SpecialInfo.stNetInfo.chCurrentIp, devs[nIndex].chModelName, devs[nIndex].chDeviceVersion,
               devs[nIndex].chManufacturerName, devs[nIndex].chManufacturerSpecificInfo, devs[nIndex].chUserDefinedName, 
               devs[nIndex].SpecialInfo.stNetInfo.chMacAddress[0], devs[nIndex].SpecialInfo.stNetInfo.chMacAddress[1],devs[nIndex].SpecialInfo.stNetInfo.chMacAddress[2], 
               devs[nIndex].SpecialInfo.stNetInfo.chMacAddress[3], devs[nIndex].SpecialInfo.stNetInfo.chMacAddress[4], devs[nIndex].SpecialInfo.stNetInfo.chMacAddress[5],
               devs[nIndex].SpecialInfo.stNetInfo.chCurrentSubNetMask, devs[nIndex].SpecialInfo.stNetInfo.chDefultGateWay, devs[nIndex].SpecialInfo.stNetInfo.chNetExport);
		}
		else if (DeviceType_USB == devs[nIndex].enDeviceType)
		{  
			sprintf(chDevInfo, "SerialNum[%s] name[%s] DeviceVersion[%s].\r\nManufacturerName[%s] ManufacturerSpecificInfo[%s] UserDefinedName[%s] VendorId[%d] ProductId[%d] enUsbProtocol[%d] DeviceGUID[%s].\r\n",
				devs[nIndex].chSerialNumber,devs[nIndex].chModelName, devs[nIndex].chDeviceVersion,
				devs[nIndex].chManufacturerName, devs[nIndex].chManufacturerSpecificInfo, devs[nIndex].chUserDefinedName, 
				devs[nIndex].SpecialInfo.stUsbInfo.nVendorId, devs[nIndex].SpecialInfo.stUsbInfo.nProductId,
				devs[nIndex].SpecialInfo.stUsbInfo.enUsbProtocol, devs[nIndex].SpecialInfo.stUsbInfo.chDeviceGUID);
		}

        int nRet = fputs(chDevInfo, pfile);
        if (0 <= nRet)
        {
            LOGD("Save Device Info Success!\r\n");
        }
        else
        {
            LOGD("Save Device Info File Failed \r\n");
        }
    }
    else
    {
        LOGD("Save Device Info Failed \r\n");
    }
    fclose(pfile);

    //打开设备
    void* handle = NULL;
    ASSERT_OK(MV3D_RGBD_OpenDevice(&handle, &devs[nIndex]));
    LOGD("OpenDevice success.");

    ASSERT_OK(MV3D_RGBD_CloseDevice(&handle));
    ASSERT_OK(MV3D_RGBD_Release());
    
    LOGD("Main done!");
    system("pause");

    return  0;
}

