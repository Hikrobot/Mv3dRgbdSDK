
#include "../common/common.hpp"

int main(int argc,char** argv)
{
    MV3D_RGBD_VERSION_INFO stVersion;
    ASSERT_OK( MV3D_RGBD_GetSDKVersion(&stVersion) );
    LOGD("dll version: %d.%d.%d", stVersion.nMajor, stVersion.nMinor, stVersion.nRevision);

    ASSERT_OK(MV3D_RGBD_Initialize());

    unsigned int nDevNum = 0;
    ASSERT_OK(MV3D_RGBD_GetDeviceNumber(DeviceType_Ethernet | DeviceType_USB, &nDevNum));
    LOGD("MV3D_RGBD_GetDeviceNumber success! nDevNum:%d.", nDevNum);
    ASSERT(nDevNum);

    // 查找设备
    std::vector<MV3D_RGBD_DEVICE_INFO> devs(nDevNum);
    ASSERT_OK(MV3D_RGBD_GetDeviceList(DeviceType_Ethernet | DeviceType_USB,&devs[0], nDevNum, &nDevNum));
    for (unsigned int i = 0; i < nDevNum; i++)
    {  
        LOG("Index[%d]. SerialNum[%s] IP[%s] name[%s].\r\n", i, devs[i].chSerialNumber, devs[i].SpecialInfo.stNetInfo.chCurrentIp, devs[i].chModelName);
    }

    LOG("Please enter the number of cameras to be connected：\n");
    int nLinkCount = 0;
    scanf("%d",&nLinkCount);

    while(nLinkCount > nDevNum || 0 >= nLinkCount)
    {
        LOG("The input is greater than the total number of devices.Please input again！\n");
        scanf("%d",&nLinkCount);
    }

    std::vector<unsigned int> nIndexArray(nLinkCount);
    LOG("Please enter the serial number of the camera to be connected：\n");
    for (int i = 0;i < nLinkCount;i++)
    {
        LOG("Please enter the serial number of No.%d camera to be connected：\n",i+1);        
        scanf("%d",&nIndexArray[i]);
        LOG("Connected camera:%d \r\n", nIndexArray[i]);

        if ((nDevNum  <= nIndexArray[i]) ||
            (0 > nIndexArray[i]))
        {
            LOG("enter error!\r\n");
        }
        else
        {
            continue;
        }
    }
 
    std::vector<void*> pHandle(nLinkCount);
    unsigned int nIndex = 0;
    for (int i = 0;i < nLinkCount;i++)
    {
        nIndex = nIndexArray[i];
        //打开设备    
        ASSERT_OK(MV3D_RGBD_OpenDevice(&pHandle[i], &devs[nIndex]));
        LOGD("OpenDevice index(%d) success.",nIndexArray[i]);

        // 开始工作流程
        ASSERT_OK(MV3D_RGBD_Start(pHandle[i]));
        LOGD("Start work success.");
    }

    LOGD("While loop to fetch frame\r\n");

    int cam_index = 0;
    MV3D_RGBD_FRAME_DATA stFrameData = {0};
    BOOL bExit_Main = FALSE;
    while (!bExit_Main)
    {
        if (cam_index >= nLinkCount)
        {
            cam_index = 0;
        }

        // 获取图像数据
        int nRet = MV3D_RGBD_FetchFrame(pHandle[cam_index], &stFrameData, 5000);
        if (MV3D_RGBD_OK == nRet)
        {
            for(int i = 0; i < stFrameData.nImageCount; i++)
            {
               LOGD("Cam:%d MV3D_RGBD_FetchFrame success: framenum (%d) height(%d) width(%d)  len (%d)!", nIndexArray[cam_index], stFrameData.stImageData[i].nFrameNum,
                stFrameData.stImageData[i].nHeight, stFrameData.stImageData[i].nWidth, stFrameData.stImageData[i].nDataLen);
            }
        }

        cam_index++;

        //按任意键退出
        if (_kbhit())
        {
            bExit_Main = TRUE;
        }
    }

    LOGD("All Cam stop work.");

    for(uint32_t i = 0; i < nLinkCount; i++)
    {
        ASSERT_OK(MV3D_RGBD_Stop(pHandle[i]));
        ASSERT_OK(MV3D_RGBD_CloseDevice(&pHandle[i]));
    }
    ASSERT_OK(MV3D_RGBD_Release());

    LOGD("Main done!");
    return  0;
}

