# Mv3dRgbdSDK

#### 介绍

RGB-D相机SDK基础版本

#### 软件架构

SDK目录说明

```
+---Documentations      SDK API Documents
+---Include             C header file
+---Lib                 dynamic link library for windows
+---Runtime             SDK Installation package 
+---Sample              sample application source code
```

#### 安装教程

1、安装RGB-D相机SDK运行时库后，运行程序目录下不需要放置库文件，可以直接使用环境变量中的库文件，程序可以直接运行；

C程序直接调用系统中的依赖文件，即可直接运行；

2、 如果环境变量中有RGB-D相机SDK，程序目录下有RGB-D相机SDK，那么程序会优先使用EXE目录下的RGB-D相机SDK和配置文件；

3、RGB-D相机SDK运行库的安装方法：

      使用管理员权限运行RGB-D相机SDK运行时库（Runtime目录下的exe）；

4、RGB-D相机SDK运行库的卸载方法：

      运行uninstall.exe（C:\Program Files (x86)\Common Files\Mv3dRgbdSDK目录）；
